# Norwegian Bokmal translation for ubuntu-calendar-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the ubuntu-calendar-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calendar-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-07 08:29+0000\n"
"PO-Revision-Date: 2019-03-17 00:00+0000\n"
"Last-Translator: Allan Nordhøy <epost@anotheragency.no>\n"
"Language-Team: Norwegian Bokmål <https://translate.ubports.com/projects/"
"ubports/calendar-app/nb/>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.1.1\n"
"X-Launchpad-Export-Date: 2017-04-05 07:14+0000\n"

#: ../qml/TimeLineBase.qml:50 ../qml/AllDayEventComponent.qml:89
msgid "New event"
msgstr "Ny hendelse"

#: ../qml/CalendarChoicePopup.qml:44 ../qml/EventActions.qml:54
msgid "Calendars"
msgstr "Kalendere"

#: ../qml/CalendarChoicePopup.qml:46 ../qml/SettingsPage.qml:51
msgid "Back"
msgstr "Tilbake"

#. TRANSLATORS: Please translate this string  to 15 characters only.
#. Currently ,there is no way we can increase width of action menu currently.
#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Sync"
msgstr "Synk."

#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Syncing"
msgstr "Synkroniserer"

#: ../qml/CalendarChoicePopup.qml:83
msgid "Add online Calendar"
msgstr "Legg til nettbasert kalender"

#: ../qml/CalendarChoicePopup.qml:184
msgid "Unable to deselect"
msgstr "Klarte ikke å fjerne markering"

#: ../qml/CalendarChoicePopup.qml:185
msgid ""
"In order to create new events you must have at least one writable calendar "
"selected"
msgstr "Du må markere minst én kalender for å kunne lage nye hendelser"

#: ../qml/CalendarChoicePopup.qml:187 ../qml/RemindersPage.qml:80
msgid "Ok"
msgstr "OK"

#. TRANSLATORS: the first argument (%1) refers to a start time for an event,
#. while the second one (%2) refers to the end time
#: ../qml/EventBubble.qml:139
msgid "%1 - %2"
msgstr "%1 – %2"

#: ../qml/RemindersModel.qml:31 ../qml/RemindersModel.qml:99
msgid "No Reminder"
msgstr "Ingen påminnelse"

#. TRANSLATORS: this refers to when a reminder should be shown as a notification
#. in the indicators. "On Event" means that it will be shown right at the time
#. the event starts, not any time before
#: ../qml/RemindersModel.qml:34 ../qml/RemindersModel.qml:103
msgid "On Event"
msgstr "Når hendelsen begynner"

#: ../qml/RemindersModel.qml:43
msgid "%1 week"
msgid_plural "%1 weeks"
msgstr[0] "%1 uke"
msgstr[1] "%1 uker"

#: ../qml/RemindersModel.qml:54
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1 dag"
msgstr[1] "%1 dager"

#: ../qml/RemindersModel.qml:65
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 time"
msgstr[1] "%1 timer"

#: ../qml/RemindersModel.qml:74
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minutt"
msgstr[1] "%1 minutter"

#: ../qml/RemindersModel.qml:104
msgid "5 minutes"
msgstr "5 minutter"

#: ../qml/RemindersModel.qml:105
msgid "10 minutes"
msgstr "10 minutter"

#: ../qml/RemindersModel.qml:106
msgid "15 minutes"
msgstr "15 minutter"

#: ../qml/RemindersModel.qml:107
msgid "30 minutes"
msgstr "30 minutter"

#: ../qml/RemindersModel.qml:108
msgid "1 hour"
msgstr "1 time"

#: ../qml/RemindersModel.qml:109
msgid "2 hours"
msgstr "2 timer"

#: ../qml/RemindersModel.qml:110
msgid "1 day"
msgstr "1 dag"

#: ../qml/RemindersModel.qml:111
msgid "2 days"
msgstr "2 dager"

#: ../qml/RemindersModel.qml:112
msgid "1 week"
msgstr "1 uke"

#: ../qml/RemindersModel.qml:113
msgid "2 weeks"
msgstr "2 uker"

#: ../qml/RemindersModel.qml:114
msgid "Custom"
msgstr "Selvvalgt"

#: ../qml/AgendaView.qml:50 ../qml/calendar.qml:333 ../qml/calendar.qml:514
msgid "Agenda"
msgstr "Agenda"

#: ../qml/AgendaView.qml:95
msgid "You have no calendars enabled"
msgstr "Du har ingen aktive kalendere"

#: ../qml/AgendaView.qml:95
msgid "No upcoming events"
msgstr "Ingen kommende hendelser"

#: ../qml/AgendaView.qml:107
msgid "Enable calendars"
msgstr "Aktiver kalendere"

#: ../qml/AgendaView.qml:199
msgid "no event name set"
msgstr "intet navn valgt"

#: ../qml/AgendaView.qml:201
msgid "no location"
msgstr "intet sted"

#: ../qml/LimitLabelModel.qml:25
msgid "Never"
msgstr "Aldri"

#: ../qml/LimitLabelModel.qml:26
msgid "After X Occurrence"
msgstr "Etter X forekomst"

#: ../qml/LimitLabelModel.qml:27
msgid "After Date"
msgstr "Etter dato"

#: ../qml/NewEventBottomEdge.qml:54 ../qml/NewEvent.qml:382
msgid "New Event"
msgstr "Ny hendelse"

#. TRANSLATORS: Please keep the translation of this string to a max of
#. 5 characters as the week view where it is shown has limited space.
#: ../qml/AllDayEventComponent.qml:148
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 hendelse"
msgstr[1] "%1 hendelser"

#. TRANSLATORS: the argument refers to the number of all day events
#: ../qml/AllDayEventComponent.qml:152
msgid "%1 all day event"
msgid_plural "%1 all day events"
msgstr[0] "%1 heldagshendelse"
msgstr[1] "%1 heldagshendelser"

#: ../qml/EditEventConfirmationDialog.qml:29 ../qml/NewEvent.qml:382
msgid "Edit Event"
msgstr "Rediger hendelse"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/EditEventConfirmationDialog.qml:32
msgid "Edit only this event \"%1\", or all events in the series?"
msgstr "Vil du redigere enkelthendelsen «%1», eller alle hendelsene i serien?"

#: ../qml/EditEventConfirmationDialog.qml:35
msgid "Edit series"
msgstr "Rediger serie"

#: ../qml/EditEventConfirmationDialog.qml:44
msgid "Edit this"
msgstr "Rediger denne"

#: ../qml/EditEventConfirmationDialog.qml:53
#: ../qml/DeleteConfirmationDialog.qml:60 ../qml/RemindersPage.qml:72
#: ../qml/NewEvent.qml:387 ../qml/OnlineAccountsHelper.qml:73
#: ../qml/ColorPickerDialog.qml:55
msgid "Cancel"
msgstr "Avbryt"

#: ../qml/ContactChoicePopup.qml:37
msgid "No contact"
msgstr "Ingen kontakt"

#: ../qml/ContactChoicePopup.qml:96
msgid "Search contact"
msgstr "Finn kontakt"

#: ../qml/DeleteConfirmationDialog.qml:31
msgid "Delete Recurring Event"
msgstr "Slett regelmessig hendelse"

#: ../qml/DeleteConfirmationDialog.qml:32
msgid "Delete Event"
msgstr "Slett hendelse"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/DeleteConfirmationDialog.qml:36
msgid "Delete only this event \"%1\", or all events in the series?"
msgstr "Vil du slette enkelthendelsen «%1», eller alle hendelsene i serien?"

#: ../qml/DeleteConfirmationDialog.qml:37
msgid "Are you sure you want to delete the event \"%1\"?"
msgstr "Er du sikker på at du vil slette hendelsen «%1»?"

#: ../qml/DeleteConfirmationDialog.qml:40
msgid "Delete series"
msgstr "Slett serie"

#: ../qml/DeleteConfirmationDialog.qml:51
msgid "Delete this"
msgstr "Slett denne"

#: ../qml/DeleteConfirmationDialog.qml:51 ../qml/NewEvent.qml:394
msgid "Delete"
msgstr "Slett"

#: ../qml/calendar.qml:74
msgid ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"
msgstr ""
"Kalenderprogrammet kan brukes med fire argumenter: «--starttime», «--"
"endtime», «--newevent» og «--eventid». Se kildekoden for fullstendige "
"kommentarer til disse"

#: ../qml/calendar.qml:341 ../qml/calendar.qml:535
msgid "Day"
msgstr "Dag"

#: ../qml/calendar.qml:349 ../qml/calendar.qml:556
msgid "Week"
msgstr "Uke"

#: ../qml/calendar.qml:357 ../qml/calendar.qml:577
msgid "Month"
msgstr "Måned"

#: ../qml/calendar.qml:365 ../qml/calendar.qml:598
msgid "Year"
msgstr "År"

#: ../qml/calendar.qml:705 ../qml/TimeLineHeader.qml:66
#: ../qml/EventDetails.qml:173
msgid "All Day"
msgstr "Hele dagen"

#: ../qml/SettingsPage.qml:49 ../qml/EventActions.qml:66
msgid "Settings"
msgstr "Innstillinger"

#: ../qml/SettingsPage.qml:72
msgid "Show week numbers"
msgstr "Vis ukenumre"

#: ../qml/SettingsPage.qml:90
msgid "Display Chinese calendar"
msgstr "Vis kinesisk kalender"

#: ../qml/SettingsPage.qml:110
msgid "Business hours"
msgstr "Åpningstider"

#: ../qml/SettingsPage.qml:211
msgid "Default reminder"
msgstr "Forvalgt påminnelse"

#: ../qml/SettingsPage.qml:255
msgid "Default calendar"
msgstr "Forvalgt kalender"

#: ../qml/RemindersPage.qml:62
msgid "Custom reminder"
msgstr "Selvvalgt påminnelse"

#. TRANSLATORS: This is shown in the month view as "Wk" as a title
#. to indicate the week numbers. It should be a max of up to 3 characters.
#: ../qml/MonthComponent.qml:293
msgid "Wk"
msgstr "Uke"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the page to choose repetition
#. and as the header of the list item that shows the repetition
#. summary in the page that displays the event details
#: ../qml/EventRepetition.qml:40 ../qml/EventRepetition.qml:167
msgid "Repeat"
msgstr "Gjenta"

#: ../qml/EventRepetition.qml:187
msgid "Repeats On:"
msgstr "Gjenta:"

#: ../qml/EventRepetition.qml:233
#, fuzzy
msgid "Interval of recurrence"
msgstr "Etter X forekomst"

#: ../qml/EventRepetition.qml:258
msgid "Recurring event ends"
msgstr "Regelmessig hendelse slutter"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the option selector to choose
#. its repetition
#: ../qml/EventRepetition.qml:282 ../qml/NewEvent.qml:775
msgid "Repeats"
msgstr "Gjentakelser"

#: ../qml/EventRepetition.qml:308
msgid "Date"
msgstr "Dato"

#: ../qml/YearView.qml:57 ../qml/WeekView.qml:60 ../qml/MonthView.qml:50
#: ../qml/DayView.qml:76
msgid "Today"
msgstr "I dag"

#: ../qml/YearView.qml:79
msgid "Year %1"
msgstr "År %1"

#: ../qml/NewEvent.qml:199
msgid "End time can't be before start time"
msgstr "En hendelse kan ikke slutte før den har begynt"

#: ../qml/NewEvent.qml:412
msgid "Save"
msgstr "Lagre"

#: ../qml/NewEvent.qml:423
msgid "Error"
msgstr "Feil"

#: ../qml/NewEvent.qml:425
msgid "OK"
msgstr "OK"

#: ../qml/NewEvent.qml:487
msgid "From"
msgstr "Fra"

#: ../qml/NewEvent.qml:503
msgid "To"
msgstr "Til"

#: ../qml/NewEvent.qml:530
msgid "All day event"
msgstr "Heldagshendelse"

#: ../qml/NewEvent.qml:553 ../qml/EventDetails.qml:37
msgid "Event Details"
msgstr "Hendelsesdetaljer"

#: ../qml/NewEvent.qml:567
msgid "Event Name"
msgstr "Hendelsesnavn"

#: ../qml/NewEvent.qml:585 ../qml/EventDetails.qml:437
msgid "Description"
msgstr "Beskrivelse"

#: ../qml/NewEvent.qml:604
msgid "Location"
msgstr "Plassering"

#: ../qml/NewEvent.qml:619 ../qml/EventDetails.qml:348
#: com.ubuntu.calendar_calendar.desktop.in.h:1
msgid "Calendar"
msgstr "Kalender"

#: ../qml/NewEvent.qml:681
msgid "Guests"
msgstr "Gjester"

#: ../qml/NewEvent.qml:691
msgid "Add Guest"
msgstr "Legg til gjest"

#: ../qml/NewEvent.qml:797 ../qml/NewEvent.qml:814 ../qml/EventDetails.qml:464
msgid "Reminder"
msgstr "Påminnelse"

#: ../qml/WeekView.qml:137 ../qml/MonthView.qml:76
msgid "%1 %2"
msgstr "%1 %2"

#: ../qml/WeekView.qml:144 ../qml/WeekView.qml:145
msgid "MMM"
msgstr "MMM"

#. TRANSLATORS: this is a time formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#. It's used in the header of the month and week views
#: ../qml/WeekView.qml:156 ../qml/MonthView.qml:81 ../qml/DayView.qml:126
msgid "MMMM yyyy"
msgstr "MMMM yyyy"

#. TRANSLATORS: W refers to Week, followed by the actual week number (%1)
#: ../qml/TimeLineHeader.qml:54
msgid "W%1"
msgstr "U%1"

#: ../qml/EventDetails.qml:40
msgid "Edit"
msgstr "Rediger"

#: ../qml/EventDetails.qml:392
msgid "Attending"
msgstr "Deltar"

#: ../qml/EventDetails.qml:394
msgid "Not Attending"
msgstr "Deltar ikke"

#: ../qml/EventDetails.qml:396
msgid "Maybe"
msgstr "Kanskje"

#: ../qml/EventDetails.qml:398
msgid "No Reply"
msgstr "Ikke svart"

#: ../qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr "Velg en konto å skape."

#: ../qml/ColorPickerDialog.qml:25
msgid "Select Color"
msgstr "Velg farge"

#: ../qml/RecurrenceLabelDefines.qml:23
msgid "Once"
msgstr "Én gang"

#: ../qml/RecurrenceLabelDefines.qml:24
msgid "Daily"
msgstr "Hver dag"

#: ../qml/RecurrenceLabelDefines.qml:25
msgid "On Weekdays"
msgstr "På ukedager"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday, Tuesday, Thursday"
#: ../qml/RecurrenceLabelDefines.qml:27
msgid "On %1, %2 ,%3"
msgstr "På %1, %2 ,%3"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday and Thursday"
#: ../qml/RecurrenceLabelDefines.qml:29
msgid "On %1 and %2"
msgstr "%1 og %2"

#: ../qml/RecurrenceLabelDefines.qml:30
msgid "Weekly"
msgstr "Ukentlig"

#: ../qml/RecurrenceLabelDefines.qml:31
msgid "Monthly"
msgstr "Månedlig"

#: ../qml/RecurrenceLabelDefines.qml:32
msgid "Yearly"
msgstr "Årlig"

#. TRANSLATORS: the argument refers to multiple recurrence of event with count .
#. E.g. "Daily; 5 times."
#: ../qml/EventUtils.qml:75
msgid "%1; %2 time"
msgid_plural "%1; %2 times"
msgstr[0] "%1; %2 gang"
msgstr[1] "%1; %2 ganger"

#. TRANSLATORS: the argument refers to recurrence until user selected date.
#. E.g. "Daily; until 12/12/2014."
#: ../qml/EventUtils.qml:79
msgid "%1; until %2"
msgstr "%1, frem til %2"

#: ../qml/EventUtils.qml:93
msgid "; every %1 days"
msgstr "; hver %1 dag"

#: ../qml/EventUtils.qml:95
#, fuzzy
msgid "; every %1 weeks"
msgstr "%1 uke"

#: ../qml/EventUtils.qml:97
msgid "; every %1 months"
msgstr "; hver %5 måned"

#: ../qml/EventUtils.qml:99
msgid "; every %1 years"
msgstr "; hvert %1 år"

#. TRANSLATORS: the argument refers to several different days of the week.
#. E.g. "Weekly on Mondays, Tuesdays"
#: ../qml/EventUtils.qml:125
msgid "Weekly on %1"
msgstr "På %1"

#: com.ubuntu.calendar_calendar.desktop.in.h:2
msgid "A calendar for Ubuntu which syncs with online accounts."
msgstr "En kalender for Ubuntu som kan synkroniseres med nettkontoer."

#: com.ubuntu.calendar_calendar.desktop.in.h:3
msgid "calendar;event;day;week;year;appointment;meeting;"
msgstr "kalender;hendelse;dag;uke;år;avtale;møte;"

#~ msgid "Show lunar calendar"
#~ msgstr "Vis månekalender"
